import "./App.css";
import AppNavbar from "./components/AppNavbar.js";
// import Home from "./pages/Home.js";
// import Courses from "./pages/Courses.js";
import { Container } from "react-bootstrap";
import Register from "./pages/Register";
import Login from "./pages/Login";

function App() {
   // const [count, setCount] = useState(false);

   // function darkMode() {
   //    setCount(!count);
   // }
   // console.log(count);

   return (
      // Fragment: <> and </>
      /* Mount component and prepare output rendering */
      <>
         <AppNavbar />
         <Container>
            {/*  <Home />
            <Courses /> */}
            {/* <Register /> */}
            <Login />
         </Container>
      </>
   );
}

export default App;
